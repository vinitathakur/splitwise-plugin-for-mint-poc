import json


def pretty_print_to_file(dict, filename):
	file = open(filename, "w")
	file.write(json.dumps(dict, indent=4))
	file.close()


def create_data_for_program():
	inverse_category_groups = {}
	category_groups = {}
	unique_categories = {}

	with open("Derived_files/changed_cat.json", "r") as f:
	    category_groups = json.load(f)

	unique_categories["all"] = list(category_groups.keys())

	for category in category_groups:
		subcat_list = category_groups[category]

		for subcat in subcat_list:
			inverse_category_groups[subcat] = category

	pretty_print_to_file(inverse_category_groups, "'Derived_files/inverse_category_groups.json")
	pretty_print_to_file(unique_categories, "Derived_files/unique_categories.json")


if __name__ == "__main__":
	create_data_for_program()