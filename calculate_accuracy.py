import numpy as np
import pandas as pd

from sklearn import model_selection, metrics
from sklearn.feature_extraction.text import TfidfVectorizer
from sklearn import ensemble


def train_model(classifier, feature_vector_train, training_labels):
    # fit the training dataset on the classifier
    classifier.fit(feature_vector_train, training_labels)
    return classifier

def calculate_accuracy(classifier, feature_vector_valid, valid_text, label_translations, valid_labels):
    # predict the labels on validation dataset
    predictions = classifier.predict(feature_vector_valid)
    accuracy_score = metrics.accuracy_score(predictions, valid_labels)
    return accuracy_score

if __name__ == "__main__":
    data = open("Derived_files/train.txt").read()
    labels, texts = [], []

    for i, line in enumerate(data.split("\n")):
        content = line.split(", ")
        texts.append(content[0].strip().lower())
        labels.append(content[1].strip())

    trainDF = pd.DataFrame()
    trainDF['text'] = texts
    trainDF['label'] = labels

    # split the dataset into training and validation datasets 
    train_x, valid_x, train_y, valid_y = model_selection.train_test_split(trainDF['text'], trainDF['label'])

    train_y_labels = []
    valid_y_labels = []

    label_id = 0
    label_dict = {}

    for elem in train_y:
        if elem not in label_dict:
            label_dict[elem] = label_id
            label_id+=1
    for elem in valid_y:
        if elem not in label_dict:
            label_dict[elem] = label_id
            label_id+=1

    for elem in train_y:
        train_y_labels.append(label_dict[elem])

    for elem in valid_y:
        valid_y_labels.append(label_dict[elem])
        
    train_y = np.array(train_y_labels)
    valid_y = np.array(valid_y_labels)

    tfidf_vect = TfidfVectorizer(analyzer="word", token_pattern=r'\w{1,}', max_features=5000)
    tfidf_vect.fit(trainDF["text"])
    xtrain_tfidf =  tfidf_vect.transform(train_x)
    xvalid_tfidf =  tfidf_vect.transform(valid_x)

    # RF on Word Level TF IDF Vectors
    classifier = train_model(ensemble.RandomForestClassifier(), xtrain_tfidf, train_y)
    accuracy = calculate_accuracy(classifier, xvalid_tfidf, valid_x, label_dict, valid_y)
    print(accuracy)
