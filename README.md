# Splitwise plugin for mint POC

### Overview
* This project aims to classify transactions on splitwise into categories defined by mint.com
* Firstly, I grouped the mint.com sub-categories to high level categories. The file mint_cat.json shows how sub-categories are combined into higher level categories
* Then, all the mint.com transactions were classified into these top level categories
* I created training data for the splitwise transactions - all classification labels should also belong to the above defined set of top level categories
* The training algorithm was chosen after trying out several algorithms and approaches from this blog post: 
* I got an accuracy of around 75% with Random forest using word-level TF-IDF, hence chose that as a classifier
* The user is asked for entering the category of the transaction if confidence of classifictaion is lesser than 75%

### Files and folders
* ```Data``` contains two sub folders - Mint and Splitwise. Mint folder contains a single ```transactions.csv``` file while the Splitwise folder contains multiple csv's obtained from Splitwise. Both folders contain sample data files. Note that there should be intersecting months between the splitwise and mint files for the code to work as expected. See the sample data for reference
* ```Derived_files``` contains all the files that were created/generated. Make sure to run ```preprocess.py``` once before starting ```main.py```
* ```Explanation``` contains some notes that were written while developing the project
* ```training_data``` contains a text file that has splitwise transactions and categories used as training data. Please use custome training data for better results
* ```calculate_accuracy.py``` prints the accuracy based on the current training data (Splits training data into training and validation sets)
* ```Experiment.ipynb``` contains random code that was tried out before creating a formal file structure

### To-do's
* Create a requirements file for python dependencies
* Setup instructions file