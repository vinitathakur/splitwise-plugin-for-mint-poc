import numpy as np
import pandas as pd

from sklearn.feature_extraction.text import TfidfVectorizer
from sklearn import ensemble


def train_model(classifier, feature_vector_train, training_labels):
    # fit the training dataset on the classifier
    classifier.fit(feature_vector_train, training_labels)
    return classifier

def create_feature_vector():
    data = open("training_data/train.txt").read()
    labels, texts = [], []

    for i, line in enumerate(data.split("\n")):
        content = line.split(", ")
        texts.append(content[0].strip().lower())
        labels.append(content[1].strip())

    trainDF = pd.DataFrame()
    trainDF['text'] = texts
    trainDF['label'] = labels

    trainDF_encoded_labels = []

    label_id = 0
    label_dict = {}

    for elem in trainDF['label']:
        if elem not in label_dict:
            label_dict[elem] = label_id
            label_id+=1
        trainDF_encoded_labels.append(label_dict[elem])

    trainDF_encoded_labels =np.array(trainDF_encoded_labels)

    tfidf_vect = TfidfVectorizer(analyzer='word', token_pattern=r'\w{1,}', max_features=5000)
    tfidf_vect.fit(trainDF['text'])
    train_text_tfidf = tfidf_vect.transform(trainDF['text'])

    return label_dict, tfidf_vect, train_text_tfidf, trainDF_encoded_labels