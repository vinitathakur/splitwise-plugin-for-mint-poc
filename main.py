import json
import pandas as pd
import numpy as np

from os import listdir
from os.path import isfile, join
from sklearn import ensemble

from train_model import train_model, create_feature_vector
from preprocess import pretty_print_to_file


"""Function that
Takes as input:
    -   A file path for the mint.com csv file
Returns :
    -   A dataframe: containing the below columns:
        Date, Description, Original Description, Amount, Transaction Type,
        Category, Account Name
    -   A set: of dates(month/year) of the transactions in the csv
"""
def get_mint_df(filepath):
    mint_data = pd.read_csv(filepath)
    mint_data.dropna()
    mint_data["Date"] = pd.to_datetime(mint_data.Date)
    mint_data = mint_data.drop(columns=["Labels", "Notes"])
    month_year_mint = set(mint_data.Date.dt.strftime("%m/%Y").unique().tolist())
    return mint_data, month_year_mint


"""Function that
Takes as input:
    -   A folder path 
    -   Name of the person for whom splitwise data is being examined
Returns:
    -   A dataframe: containing the below columns:
        Date, Description, Category, Cost, Currency,
        <Name>
    -   A set: of dates(month/year) of the transactions in
        multiple splitwise csv files
"""
def get_splitwise_df(folder_path, name):
    all_sets = []
    all_frames = []

    files = []
    for file in listdir(folder_path):
        full_path = join(folder_path, file)
        if isfile(full_path):
            files.append(full_path)

    files = list(filter(lambda a: not(a.endswith(".DS_Store")), files))

    for file in files:
        splitwise_data = pd.read_csv(file)
        splitwise_data.dropna()
        splitwise_data = splitwise_data[["Date", "Description", "Category", "Cost", "Currency", name]]
        splitwise_data["Date"] = pd.to_datetime(splitwise_data.Date)
        month_year_set = set(splitwise_data.Date.dt.strftime("%m/%Y").unique().tolist())
        all_sets.append(month_year_set)
        all_frames.append(splitwise_data)

    return pd.concat(all_frames, sort=False), set.union(*all_sets)


""" Function to sort dates by year first and then months"""
def sort_dates(str_date):
    splitted = str_date.split("/")
    return (splitted[1], splitted[0])


""" Function to filter a dataframe based on specified date """
def filter_df_on_Date(date_str, df):
    splitted = date_str.split("/")
    month = int(splitted[0])
    year = int(splitted[1])
    df = df[(df['Date'].dt.month == month) & (df['Date'].dt.year == year)]
    return df


""" Function to check if a description string exists in the knowledge base """
def check_in_kb(desc):
    if desc in kb:
        return True
    else:
        return False


if __name__=="__main__":
    inverse_category_groups = {}
    category_set = []

    with open("Derived_files/inverse_category_groups.json", "r") as f:
        inverse_category_groups = json.load(f)

    with open("Derived_files/unique_categories.json", "r") as f:
        category_set = json.load(f)["all"]

    """ kb is the knowledge base that is being used 
     to store categories of transactions that occur for
     the first time. This way when we see the exact same 
     transaction next time, we can rely on the knowledge base
     to fetch the correct category quickly """
    kb = {}

    label_dict, tfidf_vect, train_text_tfidf, trainDF_encoded_labels = create_feature_vector()
    trained_classifier = train_model(ensemble.RandomForestClassifier(), train_text_tfidf, trainDF_encoded_labels)

    inverted_label_dict = {}

    for key in label_dict.keys():
        inverted_label_dict[label_dict[key]] = key

    cat_expenses = {}

    name = input("What is your name as it appears in the splitwise csv files ? \n")

    with open("Derived_files/kb.json") as f:
        kb = json.load(f)

    print("Parsing available data..")
    mint_df, month_year_mint = get_mint_df("Data/Mint/transactions.csv")

    # Read the name from a file
    splitwise_df, month_year_splitwise = get_splitwise_df("Data/Splitwise", name)
    available_months = list(set.intersection(month_year_mint, month_year_splitwise))
    available_months = sorted(available_months, key=sort_dates)

    if len(available_months) == 0:
        print("Sorry! Please check your data")
    else:
        print("Choose from the below months:")
        i = 1
        for item in available_months:
            print(i,". ", item)
            i+=1

        chosen_month_num = input("Please enter a number :\n")
        chosen_month = available_months[int(chosen_month_num) - 1]

        filtered_mint_df = filter_df_on_Date(chosen_month, mint_df)
        filtered_splitwise_df = filter_df_on_Date(chosen_month, splitwise_df)

        for i, row in filtered_mint_df.iterrows():
            if row["Category"] in inverse_category_groups:
                filtered_mint_df.at[i, "Category"] = inverse_category_groups[row["Category"]]

        for  i, row in filtered_splitwise_df.iterrows():
            category = ""
            if check_in_kb(row["Description"]):
                category = kb[row["Description"]]
            else:
                tfidf_classification_vect = tfidf_vect.transform(np.array([row["Description"]]))
                probs = trained_classifier.predict_proba(tfidf_classification_vect[0])
                probs = probs[0]

                max = 0
                max_index = -1
                for j in range(len(probs)):
                    if probs[j] > max:
                        max = probs[j]
                        max_index = j

                if max >= 0.75:
                    category = inverted_label_dict[max_index]
                else:
                    print("What category does ", row["Description"], " belong to ? \n")
                    print("Here is a list of valid categories: ")
                    for key in sorted(inverted_label_dict.keys()):
                        print(key + 1, ". ", inverted_label_dict[key])

                    category_num = input("Please enter an integer: \n")
                    category_num = int(category_num) - 1
                    category = inverted_label_dict[category_num]
                    with open("training_data/train.txt", "a") as train_data:
                        train_data.write("\n" + row["Description"] + ", " + category)

                kb[row["Description"]] = category

            print(row["Description"], " - falls under - ", category)

            amount = float(row[name])

            # ttype is the transaction type - can be credit or debit
            ttype = ""

            if amount > 0:
                ttype = "credit"
            else:
                ttype = "debit"

            amount = abs(amount)

            if row["Currency"] == "USD":
                mint_dict = {"Date" : row["Date"], "Description" : row["Description"],"Original Description" \
                : "", "Amount" : amount, "Transaction Type" : ttype, "Category" : category, "Account Name" : "Splitwise"}
                filtered_mint_df = filtered_mint_df.append(mint_dict, ignore_index=True)

        # Save everything to the knowledge base
        pretty_print_to_file(kb, "Derived_files/kb.json")

        filtered_mint_df["Amount"] = filtered_mint_df["Amount"].apply(pd.to_numeric, errors="ignore")

        total_expenses = 0
        for cat in category_set:
            # These are hard coded categories that are not being looked at!
            if cat!="Income" and cat!="Credit Card Payment" and cat!="Transfer":
                cat_debit = filtered_mint_df[(filtered_mint_df["Category"] == cat) & (filtered_mint_df["Transaction Type"] == "debit")]["Amount"].sum()
                cat_credit = filtered_mint_df[(filtered_mint_df["Category"] == cat) & (filtered_mint_df["Transaction Type"] == "credit")]["Amount"].sum()

                cat_expenses[cat] = cat_debit - cat_credit
                total_expenses += cat_expenses[cat]

        print(cat_expenses)
        print("Your total expenses are :", total_expenses)